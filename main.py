import urllib
import pprint

import pandas as pd
import requests

base_url = 'https://api.e-stat.go.jp/rest/3.0/app/json/getStatsList?'
parameters = {
    'appId': '6a099aadae0944fdd0b5333ef5611736c0ab637e',
    'lang': 'J',
    'explanationGetFlg': 'N',
    'statsNameList': 'Y',
    'limit': 100,
}
url = base_url + urllib.parse.urlencode(parameters, safe=',')


# def call_stats_name_list():
#     response = requests.get(url)
#     list_info = response.json()[
#         'GET_STATS_LIST']['DATALIST_INF']['LIST_INF']

#     data_marged = pd.DataFrame()
#     for info in list_info:
#         coverted_dict = pd.json_normalize(info)
#         data_marged = pd.concat([data_marged, coverted_dict])

#     print(data_marged)
#     pprint.pprint(data_marged.to_dict('list'))

def call_stats_data():
    url = 'https://api.e-stat.go.jp/rest/3.0/app/json/getStatsData?appId=6a099aadae0944fdd0b5333ef5611736c0ab637e&lang=J&statsDataId=0003228116&limit=100'
    response = requests.get(url)
    jsoned = response.json()
    values = jsoned[
        'GET_STATS_DATA']['STATISTICAL_DATA']['DATA_INF']['VALUE']

    data_marged = pd.DataFrame()
    for info in values:
        coverted_dict = pd.json_normalize(info)
        data_marged = pd.concat([data_marged, coverted_dict])

    # マスタ部分を扱い易い形に変換
    class_obj_list = jsoned['GET_STATS_DATA']['STATISTICAL_DATA']['CLASS_INF']['CLASS_OBJ']
    class_obj_dct = {}
    for dct_class_obj in class_obj_list:
        dct_tmp = {}
        # 辞書の場合と、複数の辞書のリストになっている場合がある
        if isinstance(dct_class_obj['CLASS'], dict):  # 辞書の場合
            code = dct_class_obj['CLASS']['@code']  # ex. 01000
            name = dct_class_obj['CLASS']['@name']  # ex. 北海道
            dct_tmp[code] = name
        elif isinstance(dct_class_obj['CLASS'], list):  # リストの場合
            for i in range(len(dct_class_obj['CLASS'])):  # リストの中身は辞書
                # なるべく上と似た書き方が出来るようにした
                code = dct_class_obj['CLASS'][i]['@code']
                name = dct_class_obj['CLASS'][i]['@name']
                dct_tmp[code] = name
        else:
            raise('想定外動作')  # 想定外なのでエラーで止まるようにしておく
        class_obj_dct[dct_class_obj['@id']] = dct_tmp  # 辞書を追加

    # マスタの適用
    for key, dct in class_obj_dct.items():
        col = '@' + key  # ex. @area
        data_marged[col] = data_marged[col].apply(lambda s: dct[s])

    # print(data_marged)
    # pprint.pprint(data_marged.to_dict('record'))
    pprint.pprint(data_marged.set_index('@time').T.to_dict('record'))


def main():
    # call_stats_name_list()
    call_stats_data()


if __name__ == "__main__":
    main()
